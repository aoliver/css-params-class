#CSS Params Class

A simple yet effective way to add variables, concatenate and compress CSS files.

####Index

* Simple Usage
* Processing multiple files
* Exporting
* Additional CSS Parameters
* Compressing output
* Addins

##Simple Usage

Installation and use is straight forward.

```css
/* 
    The vars
*/
@@bg: #eeeeee;
@@fg: #333333;
@@margin: 20px;

body{
    background-color: @@bg;
    margin: @@margin;
    color: @@fg;
    
}

.content{
    background-color: @@bg;
    color: @@fg;
}
```

```php
<?php
    require 'cssparams.php';
    $cssP = new cssparams;
    echo $cssP->process_files('styles.css');
?>
```

The above example will echo out the processed CSS file 'styles.css'.


###Processing multiple files

```php
<?php
    require 'cssparams.php';
    $cssP = new cssparams;
    echo $cssP->process_files('styles.css, styles-two.css');
?>
```

###Exporting

```php
<?php
    require 'cssparams.php';
    $cssP = new cssparams;
    echo $cssP->concat_files('styles.css, styles-two.css',[],'exported.css');
?>
```

###Additional CSS Parameters

```php
<?php
    require 'cssparams.php';
    $cssP = new cssparams;
    $cssP->compress = true;
    echo $cssP->concat_files('styles.css, styles-two.css',[],'exported.css');
?>
```


###Compressing output

```php
<?php
    $addrData = [];
    $addrData['fontsize'] = '12px';

    require 'cssparams.php';
    $cssP = new cssparams;
    echo $cssP->concat_files('styles.css, styles-two.css',$addrData,'exported.css');
?>
```

###Addins

CSS variables and addins

```css
@@col1: #333333;
@@col2: #ffffff;

@@fontsize{
    font-size: 20px;
    padding-bottom: 40px;
    color: @@col1;
}

@@col-group-one{
    background-color: @@col1;
    color: @@col2;
}

@@gen-padding: 20px;
@@wrap-width: 1200px;
```

CSS usage

```css
header{
    padding: @@gen-padding;
    @@col-group-one;
}
```