<?php
    /*
        CSS Params Class
        @license: MIT - http://opensource.org/licenses/MIT
        @version: 1.0.2
        @author: Alex Oliver
        @Repo: https://bitbucket.org/aoliver/css-params-class

        Copyright (c) 2016 Alex Oliver
    */
    class cssparams{

        var $processedString = '',
        $stylesPath = '',
        $compress = false;

        function __construct($inputString = false){
            if($inputString && strlen($inputString) > 0){
                $this->processedString = $this->process_string($inputString, []);
            }
        }

        public function process_string($theString = '', $data = []){

            //find params
            preg_match_all('/@@(.*):(.*);/', $theString, $matches, PREG_SET_ORDER);

            //find addins
            preg_match_all('/(@@(.*)(.*){)((.|\n)*?)(})/', $theString, $addinmatches, PREG_SET_ORDER);

            //clean document of matches
            $theString = preg_replace('/(@@(.*)(.*){)((.|\n)*?)(})/', '', $theString);
            $theString = preg_replace('/@@(.*):(.*)/', '', $theString);
            $theString = preg_replace('/\/\/(.*)/', '', $theString);

            //comments
            //$theString = preg_replace('/\/\*(.*)\*\//s', '', $theString);
            $theString = preg_replace('/\/\*(.*)\*\//', '', $theString);

            if(is_array($data)){
                foreach($data as $key=>$val){
                    $matches[] = [
                        0 => '@@'.$key,
                        1 => $key,
                        2 => $val
                    ];
                }
            }

            //cycle through addins and replace
            foreach($addinmatches as $thisAddin){
                if(count($thisAddin) >= 7){
                    $theString = str_replace('@@'.$thisAddin[2], trim($thisAddin[4]), $theString);
                }
            }

            //cycle through params and replace
            foreach($matches as $thisMatch){
                if(count($thisMatch) >= 3){
                    $theString = str_replace('@@'.$thisMatch[1], trim($thisMatch[2]), $theString);
                }
            }

            //clean up
            $theString = str_replace(';;', ';', $theString);
            $theString = preg_replace('/@@(.*);/', '', $theString);

            //return formatted css
            if($this->compress){
                return str_replace(["\n",'  '], '', $theString);
            } else {
                return str_replace("\n\n\n", "", $theString);
            }
        }

        public function process_files($theFiles = '', $data = []){
            $processString = '';
            foreach(explode(',', $theFiles) as $thisFile){
                $thisFile = $this->stylesPath.trim($thisFile);
                if(file_exists($thisFile)){
                    $processString .= "\n".file_get_contents($thisFile);
                }
            }

            if(strlen($processString) > 0){
                return $this->process_string($processString, $data);
            }
        }

        public function concat_files($theFiles = '', $data = [], $filename = 'exported/export.css'){
            $this->processedString = $this->process_files($theFiles, $data);
            return file_put_contents($filename, $this->processedString);
        }
    }
?>